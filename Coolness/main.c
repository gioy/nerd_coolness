//
//  main.c
//  Coolness
//
//  Created by iOS Dev on 9/30/13.
//  Copyright (c) 2013 iOS Dev. All rights reserved.
//

#include <stdio.h>



// readline() somehow breaks badly when invoked.
int main(int argc, const char * argv[])
{
    printf("Who is cool? ");
    const char *name = readline(NULL);
    
    printf("%s is cool!\n\n", name);
    
    return 0;
}

